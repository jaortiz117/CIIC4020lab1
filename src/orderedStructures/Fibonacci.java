package orderedStructures;

public class Fibonacci extends Progression {

	private double prev; 
	
	public Fibonacci() { 
		this(1); 
		prev = 0; 
	}
	private Fibonacci(double first) {
		super(first);
	}

	@Override
	public double nextValue() throws IllegalStateException{
		if(!super.getFirstValueRun())
			throw new IllegalStateException("firstValue() method has not been executed first");
		
        // add the necessary code here
		//...
		double temp = current;
		current += prev;
		prev = temp;
		return current;
	}
	
	@Override	
	public double firstValue() { 
		double value = super.firstValue(); 
		prev = 0; 
		return value; 
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(o instanceof Fibonacci) {
			Fibonacci oa = (Fibonacci) o;
			
			if(this.first == oa.first) {
				return true;
			}
		}
		return false;
		
	}

}
