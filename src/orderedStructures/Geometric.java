package orderedStructures;

public class Geometric extends Progression {

	private double commonFactor; 
	
	public Geometric(double firstValue, double commonFactor) { 
		super(firstValue); 
		this.commonFactor = commonFactor; 
	}
	
	@Override
	public double nextValue() throws IllegalStateException{
		if(!super.getFirstValueRun())
			throw new IllegalStateException("firstValue() method has not been executed first");
		
		current = current * commonFactor; 
		return current;
	}
	
	public double getCommonFactor(){
		return this.commonFactor;
	}

	public String toString(){		
		return "Geom("+(int)this.firstValue()+", "
				+(int)this.getCommonFactor()+")";
	}
	
	@Override
	public double getTerm(int n) throws IndexOutOfBoundsException { 
		if (n <= 0) 
			throw new IndexOutOfBoundsException("printAllTerms: Invalid argument value = " + n); 
		
		return this.firstValue()*(Math.pow(this.commonFactor, n-1));
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(o instanceof Geometric) {
			Geometric oa = (Geometric) o;
			
			if(this.first == oa.first
					&& this.getCommonFactor() == (oa.getCommonFactor())) {
				return true;
			}
		}
		return false;
		
	}

}
