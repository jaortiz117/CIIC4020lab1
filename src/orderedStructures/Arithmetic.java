package orderedStructures;

import interfaces.Combinable;

public class Arithmetic extends Progression implements Combinable{
	private double commonDifference; 
	
	public Arithmetic(double firstValue, double commonDifference) { 
		super(firstValue); 
		this.commonDifference = commonDifference; 
	}
	
	@Override
	public double nextValue() throws IllegalStateException{
		if(!super.getFirstValueRun())
			throw new IllegalStateException("firstValue() method has not been executed first");
		
		current = current + commonDifference; 
		return current;
	}
	
	public double getCommonDifference(){
		return this.commonDifference;
	}

	public String toString(){		
		return "Arith("+(int)this.firstValue()+", "
				+(int)this.getCommonDifference()+")";
	}
	
	@Override
	public double getTerm(int n) throws IndexOutOfBoundsException { 
		if (n <= 0) 
			throw new IndexOutOfBoundsException("printAllTerms: Invalid argument value = " + n); 
		
		return this.firstValue()+((n-1)*this.getCommonDifference());
	}
	
	@Override
	public boolean equals(Object o) {
		
		if(o instanceof Arithmetic) {
			Arithmetic oa = (Arithmetic) o;
			
			if(this.first == oa.first
					&& this.getCommonDifference() == (oa.getCommonDifference())) {
				return true;
			}
		}
		return false;
		
	}

	@Override
	public Progression add(Progression p) {
		Arithmetic other = (Arithmetic) p;
		
		return new Arithmetic(this.first+other.first, 
				this.commonDifference+other.getCommonDifference());
	}

	@Override
	public Progression substract(Progression p) {
		Arithmetic other = (Arithmetic) p;

		return new Arithmetic(this.first-other.first, 
				this.commonDifference-other.getCommonDifference());
	}
}
