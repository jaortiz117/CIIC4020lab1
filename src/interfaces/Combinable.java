package interfaces;

import orderedStructures.Progression;

public interface Combinable {
	
	public Progression add(Progression p);
	public Progression substract(Progression p);
}
