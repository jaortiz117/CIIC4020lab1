package TesterClasses;

import orderedStructures.Arithmetic;
import orderedStructures.Fibonacci;
import orderedStructures.Geometric;
import orderedStructures.Progression;

public class MyTester {

	public static void main(String[] args) {
		Progression arith = new Arithmetic(1,1);
		Progression arith2 = new Arithmetic(1,1);
		Progression arith3 = new Arithmetic(1,2);
		
		Progression geo = new Geometric(1,1);
		Progression geo2 = new Geometric(1,1);
		Progression geo3 = new Geometric(1,2);
		
		Progression fib = new Fibonacci();
		Progression fib2 = new Fibonacci();
		
		System.out.println("Arithmetic: " + arith.equals(arith2));
		System.out.println("Arithmetic: "+ arith.equals(arith3));
		
		System.out.println("Geometric: "+ geo.equals(geo2));
		System.out.println("Geometric: "+ geo.equals(geo3));

		System.out.println("Fibonacci: "+ fib.equals(fib2));
		
		System.out.println("------------------------------------------");
		System.out.println("Combinable addition testing");
		System.out.println("arith: 10: " + arith.getTerm(10));
		System.out.println("arith2: 10: " + arith3.getTerm(10));
		
		Progression newArith = ((Arithmetic) arith).add(arith3);
		
		System.out.println("\n After Combinable add: "+ newArith.getTerm(10));
		
		newArith = ((Arithmetic) arith).substract(arith3);
		
		System.out.println("\n After Combinable substract: "+ newArith.getTerm(10));

	}

}
